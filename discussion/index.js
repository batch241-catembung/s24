//alert("hello world");


//es6 updates


//exponent operator

//old
console.log("result of old:");
const oldNum = Math.pow(8, 2);
console.log(oldNum);

//new
console.log("result of es6:");
const newNum = 8 **2;
console.log(newNum);

console.log("");
//template literals
/*
	allows us to write string w/o using concatinate operator
*/

let studentsName = 'Roland';

//pre-template
console.log("pre-template literal");
console.log("hello " +studentsName+ "welcome to programming");

//template literal string
console.log("template literal");
console.log(`hello ${studentsName}!welcome to programming`)

//multi-line template literal
const message = `
${studentsName} attended a math comptetition,
He won it by solving the problme 8**2 with the solution of 
${newNum}
`

console.log(message);

/*
	template literal allow us to write string with embeded js expression 

	${} are used to inclide javascript expression in string using template literals
*/

const interestRate =.1;
const principla = 1000;

console.log(`the interest rate on your savings is:  ${principla *interestRate}`);



//array destructuring

/*
	allow us to unpack elemetns on arrays into distinct variables 

	allow us to name array elements w/ variables instead of using inex numbers

	syntax
		let {variableName, variableName, variableName } = array
*/

console.log("array destructuring");

const fullName = ['jeru', 'neru', 'palma'];
//pre array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`hello ${fullName[0]} ${fullName[1]} ${fullName[2]}`);
//array destructuring
console.log("pre array destructuring");
const [fname, mname, lname] =fullName
console.log(fname);
console.log(mname);
console.log(lname);
console.log(`hello ${fname} ${mname} ${lname}  ! nice to meet you`);



//object destructuring
/*
	allows us to unpack properties on the object distinct values
	
	shorteds accesing properties from object

	let {propertyName, propertyName} =object;
*/

const person = {
	givenName: "jane",
	maideName: "dela",
	familyName: "cruz"
}

//pre object destructuring

console.log(person.givenName);
console.log(person.maideName);
console.log(person.familyName);
console.log(`hello ${person.givenName} ${person.maideName} ${person.familyName}`);
//object destructuring
const  {givenName, maideName, familyName} =person

console.log(givenName);
console.log(maideName);
console.log(familyName);
console.log(`hello ${givenName} ${maideName}${familyName}`);
console.log("");


//arrow functions
/*
	compact alternative syntax to traditional functions

	useful for cre4ating code snippets creating functions will not be reused in any other portion s of the code 

	adheres to rhe DRY principle (dont repeat your self) principle where  there is no longer a need to create a new function and think aof a name for functions that will only be used in certade code snippets.


*/
const hello = () => {
	return "good morning batch 241";
};

/*
	syntax
	let variableName = (parameterA, parameterB) => {
	console.log();
	}
	
*/

const printFullname = (fristN, middleN, lastN) =>{
	console.log(`${fristN} ${middleN} ${lastN}`);
};

printFullname('john', 'D', 'smith' );
printFullname('eric', 'A', 'andales' );

const students =['john', 'jane', 'smith'];
//arrow function with loops

//old
console.log('old');
students.forEach(function(student){
	console.log(`${student} is a stundent`);
})

console.log('new');
students.forEach((student) => {
	console.log(`${student} is a stundent`)
})

let friend = {
	talk: function(person){
		console.log(`hey ${person}`);
	}
}

//implicit return statement
/*
	there are instance when yo can omit return statement,

	this work because w/o the return statemts js adds for it for the result of the function
*/

const add = (x, y) =>{
	return x+y;
}
let total = add(1,2)
console.log(total)

const subtract = (x, y ) => x-y
let diff = subtract (3,1)
//console.log(diff())
console.log("")


//default argument value
/*
	provides default argument if none is porvided when the function is invoked
*/

const greet = (name = 'User') =>{
	return `goodmorning, ${name }`
}

console.log(greet());

//clas based object blueprints 
/*
	allows creation instatntaion of objcet as blueprints
*/

//creating a class
/*
	the constructor function is a special method of class for creating initializing an object for the class

	the "this" keyword refers to the propertied initialized from the inside the class

	syntax
	class className {
		constructor(objectPorpertyA, objectPorpertyb){
	this.objectPorpertyA = objectPorpertyA;
	this.objectPorpertyB = objectPorpertyB;
		}
	}
*/

class Car {
	constructor (brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

/*
	the "new" keyword  operator creates new object with the given arguments as the values

	no arguments provided wil create an object w/o any values assign to its properties 

	syntax 
		let variableName = new className()
*/
console.log("")
console.log("class blue prints")
let myCar = new Car()
console.log(myCar)

// console.log(`console`)
// console.log(console)
// console.log(window)
//assigning properties after creation of an object 

myCar.brand = 'ford'
myCar.name = 'ranger raptor'
myCar.year = 2021
console.log(myCar)


//instantiate a new object 
const myNewCar = new Car('toyota', 'vios', 2021)

console.log(myNewCar)

















